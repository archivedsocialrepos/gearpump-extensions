import sbt.Keys.libraryDependencies
import sbt._

name := "gearpump-extensions"

val gearpumpVersion = "0.8.3"
lazy val commonSettings = Seq(
  organization := "co.social",
  version := "0.3.0-SNAPSHOT",
  scalaVersion := "2.11.8",
  libraryDependencies ++= Seq(
    "org.apache.gearpump" %% "gearpump-core" % gearpumpVersion % Provided,
    "org.apache.gearpump" %% "gearpump-streaming" % gearpumpVersion % Provided,
    "org.scalatest" %% "scalatest" % "latest.release" % Test
  )
)


lazy val redis = project.in(file("redis")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val rabbitmq = project.in(file("rabbitmq")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val elasticsearch = project.in(file("elasticsearch")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val influxdb = project.in(file("influxdb")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val sendgrid = project.in(file("sendgrid")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val slack = project.in(file("slack")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val twitter4j = project.in(file("twitter4j")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val tracking = project.in(file("tracking")).settings(commonSettings: _*).addSbtFiles(file("build.sbt"))

lazy val root = project.in(file("."))
    .settings{
      publishArtifact := false
      publish := { }
      publishLocal := { }
    }
    .aggregate(redis, rabbitmq, elasticsearch, influxdb, sendgrid, slack, twitter4j, tracking)
