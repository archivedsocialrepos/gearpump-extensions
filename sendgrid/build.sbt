name := "gearpump-extensions-sendgrid"

libraryDependencies ++= Seq(
  "com.sendgrid" % "sendgrid-java" % "3.0.+"
)
