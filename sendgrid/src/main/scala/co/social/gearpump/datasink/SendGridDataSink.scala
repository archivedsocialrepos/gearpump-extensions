package co.social.gearpump.datasink

import java.io.{PrintWriter, StringWriter}

import com.sendgrid._
import org.apache.gearpump.Message
import org.apache.gearpump.streaming.sink.DataSink
import org.apache.gearpump.streaming.task.TaskContext
import org.slf4j.Logger

class SendGridDataSink(sgApiKey: String, sgGroupId: Int) extends DataSink {

  private var LOG: Logger = _
  private var sendGrid: SendGrid = _

  override def open(context: TaskContext): Unit = {
    LOG = context.logger
    try {
      sendGrid = new SendGrid(sgApiKey)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in opening connection to SendGrid: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def write(message: Message): Unit = {
    try {
      val mail = message.msg.asInstanceOf[Mail]
      mail.asm = new ASM
      mail.asm.setGroupId(sgGroupId)
      mail.asm.setGroupsToDisplay(Array(sgGroupId))

      val request = new Request
      request.method = Method.POST
      request.endpoint = "mail/send"
      request.body = mail.build
      val response = sendGrid.api(request)

      LOG.info("SendGrid responded with StatusCode {}, Header: '{}', Body: '{}' !", response.statusCode.toString, response.headers.toString, response.body)
    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in sending email by SendGrid: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def close(): Unit = { }

}
