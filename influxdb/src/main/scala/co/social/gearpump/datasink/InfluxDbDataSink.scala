package co.social.gearpump.datasink

import java.io.{PrintWriter, StringWriter}

import org.apache.gearpump.Message
import org.apache.gearpump.streaming.sink.DataSink
import org.apache.gearpump.streaming.task.TaskContext
import org.influxdb.dto.Point
import org.influxdb.{InfluxDB, InfluxDBFactory}
import org.slf4j.Logger

class InfluxDbDataSink(database: String,influxUrl: String = "http://localhost:8086/",
                       influxUser: String = "root", influxPass: String = "root", retentionPolicy: String = "autogen") extends DataSink {

  private var LOG: Logger = _
  private var influxDb: InfluxDB = _

  override def open(context: TaskContext): Unit = {
    LOG = context.logger
    try {
      influxDb = InfluxDBFactory.connect(influxUrl, influxUser, influxPass)
      influxDb.createDatabase(database)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in opening connection to InfluxDb: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def write(message: Message): Unit = {
    try {
      val point = message.msg.asInstanceOf[Point]
      influxDb.write(database, retentionPolicy, point)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in writing to InfluxDb: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def close(): Unit = { }

}
