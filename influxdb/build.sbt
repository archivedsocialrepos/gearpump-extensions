name := "gearpump-extensions-influxdb"

libraryDependencies ++= Seq(
  "org.influxdb" % "influxdb-java" % "2.3"
)
