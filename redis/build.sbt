name := "gearpump-extensions-redis"

libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.2"
)
