package co.social.gearpump.task.mock

import com.redis.{RedisClient, RedisClientPool, RedisCommand}
import org.apache.commons.pool.impl.GenericObjectPool

class RedisClientPoolMock(redis: RedisCommand) extends RedisClientPool("mockhost", 6379) {

  class PoolMock extends GenericObjectPool[RedisClient] {
    val clientMock = new RedisClientMock(redis)

    override def borrowObject(): RedisClient = clientMock
  }

  override val pool: GenericObjectPool[RedisClient] = new PoolMock

}
