package co.social.gearpump.task

import com.redis.{RedisClient, RedisClientPool, RedisCommand}
import com.typesafe.config.ConfigFactory
import org.apache.gearpump.cluster.UserConfig


trait TaskWithRedis {

  def buildRedisClient(): RedisCommand = {
    val config = ConfigFactory.load()
    buildRedisClient(config.getString("redis.host"),
      config.getInt("redis.port"))
  }

  def buildRedisClient(userConf: UserConfig): RedisCommand =
    buildRedisClient(
      userConf.getString("redis.host").get,
      userConf.getInt("redis.port").get)

  def buildRedisClient(redisHost: String, redisPort: Int): RedisCommand =
    new RedisClient(redisHost, redisPort)

  def buildRedisClientPool(): RedisClientPool = {
    val config = ConfigFactory.load()
    buildRedisClientPool(config.getString("redis.host"),
      config.getInt("redis.port"))
  }

  def buildRedisClientPool(userConf: UserConfig): RedisClientPool =
    buildRedisClientPool(
      userConf.getString("redis.host").get,
      userConf.getInt("redis.port").get)

  def buildRedisClientPool(redisHost: String, redisPort: Int): RedisClientPool =
    new RedisClientPool(redisHost, redisPort)
}
