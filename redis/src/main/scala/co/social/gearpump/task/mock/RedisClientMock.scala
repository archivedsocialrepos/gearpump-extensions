package co.social.gearpump.task.mock

import com.redis.{RedisClient, RedisCommand}
import com.redis.serialization.{Format, Parse}

class RedisClientMock(redis: RedisCommand) extends RedisClient {

  override def initialize: Boolean = true

  /** **
    * Operations
    */
  override def keys[A](pattern: Any)(implicit format: Format, parse: Parse[A]): Option[List[Option[A]]] = redis.keys(pattern)

  override def set(key: Any, value: Any)(implicit format: Format): Boolean = redis.set(key, value)

  override def get[A](key: Any)(implicit format: Format, parse: Parse[A]): Option[A] = redis.get(key)

  override def rename(oldkey: Any, newkey: Any)(implicit format: Format): Boolean = redis.rename(oldkey, newkey)

  override def exists(key: Any)(implicit format: Format): Boolean = redis.exists(key)

  override def del(key: Any, keys: Any*)(implicit format: Format): Option[Long] = redis.del(key, keys)


  /** **
    * SET Operations
    */
  override def sadd(key: Any, value: Any, values: Any*)(implicit format: Format): Option[Long] = redis.sadd(key, value, values)

  override def srem(key: Any, value: Any, values: Any*)(implicit format: Format): Option[Long] = redis.srem(key, value, values)

  override def sismember(key: Any, value: Any)(implicit format: Format): Boolean = redis.sismember(key, value)

  override def smembers[A](key: Any)(implicit format: Format, parse: Parse[A]): Option[Set[Option[A]]] = redis.smembers(key)


  /****
    * HASH Operations
    */
  override def hset(key: Any, field: Any, value: Any)(implicit format: Format): Boolean = redis.hset(key, field, value)

  override def hget[A](key: Any, field: Any)(implicit format: Format, parse: Parse[A]): Option[A] = redis.hget(key, field)

  override def hdel(key: Any, field: Any, fields: Any*)(implicit format: Format): Option[Long] = redis.hdel(key, field, fields)

  override def hkeys[A](key: Any)(implicit format: Format, parse: Parse[A]): Option[List[A]] = redis.hkeys(key)

}
