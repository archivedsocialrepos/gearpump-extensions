package co.social.gearpump.task

import com.typesafe.config.ConfigFactory
import org.apache.gearpump.cluster.UserConfig
import twitter4j.api.TweetsResources
import twitter4j.{Twitter, TwitterFactory}
import twitter4j.conf.ConfigurationBuilder


trait TaskWithTwitter4j {

  def buildTwitterClient(): Twitter = {
    val config = ConfigFactory.load()
    buildTwitterClient(
      config.getString("twitter.consumerKey"),
      config.getString("twitter.consumerSecret"),
      config.getString("twitter.token"),
      config.getString("twitter.tokenSecret"))
  }

  def buildTwitterClient(userConf: UserConfig): Twitter =
    buildTwitterClient(
      userConf.getString("twitter.consumerKey").get,
      userConf.getString("twitter.consumerSecret").get,
      userConf.getString("twitter.token").get,
      userConf.getString("twitter.tokenSecret").get)

  def buildTwitterClient(consumerKey: String, consumerSecret: String, token: String, tokenSecret: String): Twitter = {
    val cb = new ConfigurationBuilder()
    cb.setDebugEnabled(true)
      .setOAuthConsumerKey(consumerKey)
      .setOAuthConsumerSecret(consumerSecret)
      .setOAuthAccessToken(token)
      .setOAuthAccessTokenSecret(tokenSecret)
    val factory = new TwitterFactory(cb.build())
    factory.getInstance()
  }

  def buildTwitterStatusClient(): TweetsResources =
    buildTwitterClient()

  def buildTwitterStatusClient(userConf: UserConfig): TweetsResources =
    buildTwitterClient(userConf)

  def buildTwitterStatusClient(consumerKey: String, consumerSecret: String, token: String, tokenSecret: String): TweetsResources =
    buildTwitterClient(consumerKey, consumerSecret, token, tokenSecret)

}
