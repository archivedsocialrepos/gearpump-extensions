name := "gearpump-extensions-rabbitmq"

libraryDependencies ++= Seq(
  "com.rabbitmq" % "amqp-client" % "3.6.+"
)
