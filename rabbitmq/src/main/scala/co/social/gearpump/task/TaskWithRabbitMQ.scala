package co.social.gearpump.task

import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import com.typesafe.config.ConfigFactory
import org.apache.gearpump.cluster.UserConfig


trait TaskWithRabbitMQ {

  def getRabbitQueue(rabbitQueue: String): (Connection, Channel) = {
    val config = ConfigFactory.load()
    getRabbitQueue(rabbitQueue,
      config.getString("rabbitmq.host"),
      config.getInt("rabbitmq.port"),
      config.getString("rabbitmq.virtualHost"),
      config.getString("rabbitmq.username"),
      config.getString("rabbitmq.password"))
  }

  def getRabbitQueue(rabbitQueue: String, userConf: UserConfig): (Connection, Channel) =
    getRabbitQueue(
      rabbitQueue,
      userConf.getString("rabbitmq.host").get,
      userConf.getInt("rabbitmq.port").get,
      userConf.getString("rabbitmq.virtualHost").get,
      userConf.getString("rabbitmq.username").get,
      userConf.getString("rabbitmq.password").get)

  def getRabbitQueue(rabbitQueue: String, rabbitMqHost: String, rabbitMqPort: Int,
                     virtualHost: String = "/", username: String = "guest", password: String = "guest"): (Connection, Channel) = {
    val factory = new ConnectionFactory()
    factory.setHost(rabbitMqHost)
    factory.setPort(rabbitMqPort)
    factory.setVirtualHost(virtualHost)
    factory.setUsername(username)
    factory.setPassword(password)
    val connection = factory.newConnection()
    val channel = connection.createChannel()

    channel.queueDeclarePassive(rabbitQueue)

    return (connection, channel)
  }

}
