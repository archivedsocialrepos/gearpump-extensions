package co.social.gearpump.datasource

import java.io.{PrintWriter, StringWriter}
import java.time.Instant

import com.rabbitmq.client._
import org.apache.gearpump.Message
import org.apache.gearpump.streaming.source.DataSource
import org.apache.gearpump.streaming.task.TaskContext
import org.slf4j.Logger

import scala.collection.mutable
import scala.concurrent.duration.DurationInt

class RabbitMQDataSource(queueName: String, rmqHost: String = "localhost", rmqPort: Int = 5672, virtualHost: String = "/",
                         username: String = "guest", password: String = "guest") extends DataSource {

  private var LOG: Logger = _

  private var connection: Connection = _
  private var channel: Channel = _

  private val queue = new mutable.Queue[Array[Byte]]()

  override def open(context: TaskContext, startTime: Instant): Unit = {
    try {
      LOG = context.logger

      val factory = new ConnectionFactory()
      factory.setHost(rmqHost)
      factory.setPort(rmqPort)
      factory.setVirtualHost(virtualHost)
      factory.setUsername(username)
      factory.setPassword(password)
      connection = factory.newConnection()
      channel = connection.createChannel()

      context.scheduleOnce(1.second)(initialize())

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in starting RabbitMQ consumer: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def read(): Message = {
    try {
      if (queue.nonEmpty)
        Message(queue.dequeue(), Instant.now())
      else
        null

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in RabbitMQ reading: " + e.getMessage)
        LOG.error(sw.toString)

        null
    }
  }

  override def close(): Unit = {
    channel.close()
    connection.close()
  }

  override def getWatermark: Instant = Instant.now()


  def initialize(): Unit = {
    try {
      channel.queueDeclarePassive(queueName)

      val consumer = new DefaultConsumer(channel) {
        override def handleDelivery(consumerTag: String, envelope: Envelope,
                                    properties: AMQP.BasicProperties, body: Array[Byte]) = {
          channel.basicAck(envelope.getDeliveryTag, false)
          queue.enqueue(body)
        }
      }
      channel.basicQos(100)
      channel.basicConsume(queueName, false, consumer)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in initializing RabbitMQ consumer: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

}
