package co.social.gearpump.datasink

import java.io.{PrintWriter, StringWriter}

import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import org.apache.gearpump.Message
import org.apache.gearpump.streaming.sink.DataSink
import org.apache.gearpump.streaming.task.TaskContext
import org.slf4j.Logger

class RabbitMQDataSink(queueName: String, rmqHost: String = "localhost", rmqPort: Int = 5672, virtualHost: String = "/",
                       username: String = "guest", password: String = "guest") extends DataSink {

  private var LOG: Logger = _
  private var connection: Connection = _
  private var producer: Channel = _

  override def open(context: TaskContext): Unit = {
    LOG = context.logger
    try {
      val factory = new ConnectionFactory()
      factory.setHost(rmqHost)
      factory.setPort(rmqPort)
      factory.setVirtualHost(virtualHost)
      factory.setUsername(username)
      factory.setPassword(password)
      connection = factory.newConnection()
      producer = connection.createChannel()

      producer.queueDeclare(queueName, true, false, false, null)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in opening RabbitMQ channel: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def write(message: Message): Unit = {
    try {
      message.msg match {
        case content: Array[Byte] =>
          producer.basicPublish("", queueName, null, content)
        case _ =>
      }

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in publishing at RabbitMQ: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def close(): Unit = {
    producer.close()
    connection.close()
  }

}
