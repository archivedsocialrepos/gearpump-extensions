package co.social.gearpump.task

import com.segment.analytics.Analytics
import com.typesafe.config.ConfigFactory
import org.apache.gearpump.cluster.UserConfig


trait TaskWithTracking {

  def buildAnalytics(): Analytics = {
    val config = ConfigFactory.load()
    buildAnalytics(config.getString("segment.writeKey"))
  }

  def buildAnalytics(userConf: UserConfig): Analytics =
    buildAnalytics(
      userConf.getString("segment.writeKey").get)

  def buildAnalytics(segmentWriteKey: String): Analytics =
    Analytics.builder(segmentWriteKey).build

}
