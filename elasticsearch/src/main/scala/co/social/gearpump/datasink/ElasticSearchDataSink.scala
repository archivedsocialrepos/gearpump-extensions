package co.social.gearpump.datasink

import java.io.{PrintWriter, StringWriter}
import java.net.InetAddress

import org.apache.gearpump.Message
import org.apache.gearpump.streaming.sink.DataSink
import org.apache.gearpump.streaming.task.TaskContext
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.common.xcontent.XContentBuilder
import org.elasticsearch.transport.client.PreBuiltTransportClient
import org.slf4j.Logger

class ElasticSearchDataSink(esTypeAndIndex: String, esHost: String = "localhost", esPort: Int = 9300) extends DataSink {

  private var LOG: Logger = _
  private var elasticClient: TransportClient = _

  override def open(context: TaskContext): Unit = {
    LOG = context.logger
    try {
      val settings = Settings.builder()
        .put("client.transport.sniff", true)
        .put("client.transport.ignore_cluster_name", true)
        .build()
      elasticClient = new PreBuiltTransportClient(settings)
        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHost), esPort))

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in opening connection to ElasticSearch: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def write(message: Message): Unit = {
    try {
      message.msg match {
        case sourceBuilder: XContentBuilder =>
          elasticClient.prepareIndex(esTypeAndIndex.toLowerCase, esTypeAndIndex.toLowerCase).setSource(sourceBuilder).get
        case json: String =>
          elasticClient.prepareIndex(esTypeAndIndex.toLowerCase, esTypeAndIndex.toLowerCase).setSource(json).get
        case json: Array[Byte] =>
          elasticClient.prepareIndex(esTypeAndIndex.toLowerCase, esTypeAndIndex.toLowerCase).setSource(json).get
        case _ =>
          None
      }

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in writing to ElasticSearch: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def close(): Unit = {
    elasticClient.close()
  }

}
