package co.social.gearpump.task

import java.net.InetAddress

import com.typesafe.config.ConfigFactory
import org.apache.gearpump.cluster.UserConfig
import org.elasticsearch.client.Client
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient


trait TaskWithElasticSearch {

  def buildElasticClient(): Client = {
    val config = ConfigFactory.load()
    buildElasticClient(
      config.getString("elasticsearch.host"),
      config.getInt("elasticsearch.port"))
  }

  def buildElasticClient(userConf: UserConfig): Client =
    buildElasticClient(
      userConf.getString("elasticsearch.host").get,
      userConf.getInt("elasticsearch.port").get)

  def buildElasticClient(elasticHost: String, elasticPort: Int): Client = {
    new PreBuiltTransportClient(Settings.EMPTY)
      .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticHost), elasticPort))
  }

}
