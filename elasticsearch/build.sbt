name := "gearpump-extensions-elasticsearch"

libraryDependencies ++= Seq(
  "org.elasticsearch" % "elasticsearch" % "5.2.+",
  "org.elasticsearch.client" % "transport" % "5.2.+",
  "org.apache.logging.log4j" % "log4j-api" % "latest.release",
  "org.apache.logging.log4j" % "log4j-core" % "latest.release"
)
