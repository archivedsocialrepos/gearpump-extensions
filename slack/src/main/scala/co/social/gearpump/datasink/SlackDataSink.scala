package co.social.gearpump.datasink

import java.io.{PrintWriter, StringWriter}

import org.apache.gearpump.Message
import org.apache.gearpump.streaming.sink.DataSink
import org.apache.gearpump.streaming.task.TaskContext
import org.slf4j.Logger

import scalaj.http.Http

class SlackDataSink extends DataSink {

  private var LOG: Logger = _

  override def open(context: TaskContext): Unit = {
    LOG = context.logger
  }

  override def write(message: Message): Unit = {
    try {
      val (webhookUrl, bodyContent) = message.msg.asInstanceOf[(String, Array[Byte])]
      val response = Http(webhookUrl).postData(bodyContent).asString

      LOG.info("Slack responded with Status {}, Header: '{}', Body: '{}' !", response.code.toString, response.headers.toString, response.body)

    } catch {
      case e: Exception =>
        val sw = new StringWriter
        e.printStackTrace(new PrintWriter(sw))
        LOG.error("Exception in sending message to Slack: " + e.getMessage)
        LOG.error(sw.toString)
    }
  }

  override def close(): Unit = { }

}
